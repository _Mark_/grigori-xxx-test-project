using Mechanic.Player.Animation;
using Service.Control.Player;
using UnityEngine;
using Zenject;

namespace Infrastructure
{
	public class MainSceneInstaller : MonoInstaller
	{
		[SerializeField] private DragInput dragInput;
		[SerializeField] private PlayerStandardControlService _playerControlService;
		[SerializeField]
		private PlayerWeaponAnimation playerWeaponAnimation;

		public override void InstallBindings()
		{
			BindStateHash();
			BindIMouseInput();
			BindIPlayerControlService();
			BindAnimationParameterHash();
			BindPlayerWeaponAnimationService();
		}

		private void BindStateHash() => 
			Container.Bind<StateHash>().FromNew().AsSingle();

		private void BindPlayerWeaponAnimationService() =>
			Container.Bind<PlayerWeaponAnimation>()
				.FromInstance(playerWeaponAnimation).AsSingle();

		private void BindAnimationParameterHash() =>
			Container.Bind<ParameterHash>().FromNew().AsSingle();

		private void BindIMouseInput() =>
			Container
				.Bind<IMouseInput>()
				.FromInstance(dragInput)
				.AsSingle();

		private void BindIPlayerControlService() =>
			Container
				.Bind<IPlayerControlService>()
				.FromInstance(_playerControlService)
				.AsSingle();
	}
}