﻿using System.Collections.Generic;
using Mechanic.Player.Animation;
using Mechanic.Player.Locomotion;
using Mechanic.Player.Locomotion.Animation;
using Mechanics.Player.Audio;
using Mechanics.Player.Weapon;
using UnityEngine;
using Zenject;

namespace Infrastructure
{
	public class PlayerInstaller : MonoInstaller
	{
		[SerializeField] private GameObject _playerRoot;
		[Space]
		[SerializeField] private PlayerKick _playerKick;
		[SerializeField] private PlayerWeapon _playerWeapon;
		[SerializeField] private PlayerLocomotion _playerLocomotion;
		[SerializeField] private PlayerAudioController _playerAudioController;
		[SerializeField] private PlayerWeaponAnimation _playerWeaponAnimation;
		[SerializeField] private PlayerWeaponIkController _playerWeaponIkController;
		[SerializeField] private PlayerAnimatorController _playerAnimatorController;

		public override void InstallBindings()
		{
			BindPlayerKick();
			BindPlayerWeapon();
			BindAudioController();
			BindWeaponCollection();
			BindPlayerLocomotion();
			BindPlayerWeaponAnimation();
			BindPlayerWeaponIkController();
			BindPlayerAnimatorController();
		}

		private void BindAudioController() => 
			Container.BindInstance(_playerAudioController).AsSingle();

		private void BindPlayerKick() =>
			Container.BindInstance(_playerKick).AsSingle();

		private void BindPlayerWeapon() =>
			Container.BindInstance(_playerWeapon).AsSingle();

		private void BindPlayerWeaponAnimation() =>
			Container.BindInstance(_playerWeaponAnimation).AsSingle();

		private void BindWeaponCollection() =>
			Container.Bind<IWeapon>().FromMethodMultiple(CreateWeaponCollection)
				.AsSingle();

		private IEnumerable<IWeapon> CreateWeaponCollection(InjectContext context)
		{
			var weapons = _playerRoot.GetComponentsInChildren<IWeapon>();
			return new List<IWeapon>(weapons);
		}

		private void BindPlayerAnimatorController() =>
			Container.Bind<PlayerAnimatorController>()
				.FromInstance(_playerAnimatorController).AsSingle();

		private void BindPlayerWeaponIkController() =>
			Container.Bind<PlayerWeaponIkController>()
				.FromInstance(_playerWeaponIkController).AsSingle();

		private void BindPlayerLocomotion() =>
			Container.Bind<PlayerLocomotion>().FromInstance(_playerLocomotion)
				.AsSingle();
	}
}