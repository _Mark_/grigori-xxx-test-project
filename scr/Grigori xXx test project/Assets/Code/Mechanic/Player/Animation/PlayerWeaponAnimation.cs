﻿using System.Collections;
using Mechanics.Player.Weapon;
using UnityEngine;
using Zenject;

namespace Mechanic.Player.Animation
{
	public class PlayerWeaponAnimation : MonoBehaviour
	{
		[SerializeField] private float _recoverIkTime = 1;
		
		private float _startIkWeight;
		private Animator _animator;
		private StateHash _stateHash;
		private ParameterHash _parameter;
		private PlayerWeaponIkController _playerWeaponIkController;

		[Inject]
		private void Construct(
			StateHash stateHash,
			ParameterHash parameter,
			PlayerWeaponIkController playerWeaponIkController)
		{
			_animator = GetComponent<Animator>();
			_parameter = parameter;
			_stateHash = stateHash;
			_playerWeaponIkController = playerWeaponIkController;
		}

		public void LaunchReload()
		{
			_startIkWeight = _playerWeaponIkController.HandsWeight;
			_playerWeaponIkController.HandsWeight = 0;
			_animator.SetBool(_parameter.ReloadWeapon, true);
		}

		public void ReloadEnd(int shortNameHash)
		{
			if(shortNameHash != _stateHash.ReloadWeaponState) return;
			
			StartCoroutine(RecoverWeightForWeaponIk());
			_animator.SetBool(_parameter.ReloadWeapon, false);
		}

		private IEnumerator RecoverWeightForWeaponIk()
		{
			float timer = 0f;
			do
			{
				var weight = timer / _recoverIkTime;
				_playerWeaponIkController.HandsWeight = weight;

				timer += Time.deltaTime;
				yield return null;
			} while (timer <= _recoverIkTime);

			_playerWeaponIkController.HandsWeight = _startIkWeight;
		}
	}
}