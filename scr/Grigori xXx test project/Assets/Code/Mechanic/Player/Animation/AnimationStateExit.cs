﻿using UnityEngine;

namespace Mechanic.Player.Animation
{
	public class AnimationStateExit : StateMachineBehaviour
	{
		private bool _isFirstCall = true;
		private IAnimationStateExitReader _animationStateExitReader;

		public override void OnStateEnter(
			Animator animator,
			AnimatorStateInfo stateInfo,
			int layerIndex)
		{
			if (_isFirstCall)
			{
				_isFirstCall = false;
				_animationStateExitReader =
					animator.GetComponent<IAnimationStateExitReader>();
			}
		}

		public override void OnStateExit(
			Animator animator,
			AnimatorStateInfo stateInfo,
			int layerIndex) =>
			_animationStateExitReader.AnimationExit(stateInfo.shortNameHash);
	}
}