﻿namespace Mechanic.Player.Animation
{
	public class Const
	{
		public static class ParameterName
		{
			public const string KICK = "kick";
			public const string JUMP = "jump";
			public const string JUMP_LANDING = "jump landing";
			public const string X_VELOCITY = "x velocity";
			public const string Y_VELOCITY = "y velocity";
			public const string RELOAD_WEAPON = "reload weapon";
			public const string FALL = "fall";
			public const string FALL_LANDING = "fall landing";
		}
		
		public static class StateName
		{
			public const string FRONT_KICK = "Front kick";
			public const string JUMP_LANDING = "Jump landing";
			public const string FALL_LANDING = "Fall landing";
			public const string RELOAD_WEAPON_STATE = "Reload weapon state";
		}
	}
}