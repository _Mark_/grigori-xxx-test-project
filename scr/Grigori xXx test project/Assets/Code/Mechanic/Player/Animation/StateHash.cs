﻿using UnityEngine;

namespace Mechanic.Player.Animation
{
	public class StateHash
	{
		public readonly int JumpLanding =
			Animator.StringToHash(Const.StateName.JUMP_LANDING);
		public readonly int FallLanding =
			Animator.StringToHash(Const.StateName.FALL_LANDING);
		public readonly int FrontKick =
			Animator.StringToHash(Const.StateName.FRONT_KICK);
		public readonly int ReloadWeaponState =
			Animator.StringToHash(Const.StateName.RELOAD_WEAPON_STATE);
	}
}