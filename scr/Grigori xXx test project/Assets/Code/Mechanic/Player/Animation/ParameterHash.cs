﻿using UnityEngine;

namespace Mechanic.Player.Animation
{
	public class ParameterHash
	{
		public readonly int XVelocity =
			Animator.StringToHash(Const.ParameterName.X_VELOCITY);
		public readonly int YVelocity =
			Animator.StringToHash(Const.ParameterName.Y_VELOCITY);
		public readonly int ReloadWeapon =
			Animator.StringToHash(Const.ParameterName.RELOAD_WEAPON);
		public readonly int Kick = 
			Animator.StringToHash(Const.ParameterName.KICK);
		public readonly int Jump =
			Animator.StringToHash(Const.ParameterName.JUMP);
		public readonly int JumpLanding =
			Animator.StringToHash(Const.ParameterName.JUMP_LANDING);
		public readonly int Fall =
			Animator.StringToHash(Const.ParameterName.FALL);
		public readonly int FallLanding =
			Animator.StringToHash(Const.ParameterName.FALL_LANDING);
	}
}