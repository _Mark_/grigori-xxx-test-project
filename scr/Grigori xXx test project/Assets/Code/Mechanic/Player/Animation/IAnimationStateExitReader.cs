namespace Mechanic.Player.Animation
{
	public interface IAnimationStateExitReader
	{
		public void AnimationExit(int shortNameHash);
	}
}