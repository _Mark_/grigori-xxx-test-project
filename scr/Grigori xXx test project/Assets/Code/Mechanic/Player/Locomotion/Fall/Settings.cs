using System;

namespace Mechanic.Player.Locomotion.Fall
{
	[Serializable]
	public class Settings
	{
		public float HorizontalSpeed;
		public float HorizontalSpeedDamper;
	}
}