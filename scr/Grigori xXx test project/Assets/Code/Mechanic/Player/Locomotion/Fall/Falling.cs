using System;
using Mechanic.Player.Locomotion.Animation;
using Service.Control.Player;
using UnityEngine;

namespace Mechanic.Player.Locomotion.Fall
{
	[Serializable]
	public class Falling
	{
		[SerializeField] private Settings _settings;
		
		private bool _isFall;
		private float _currentHorizontalSpeed;
		private Transform _camera;
		private Transform _transform;
		private Rigidbody _rigidbody;
		private FallingHandler _fallingHandler;
		private IGroundDetector _groundDetector;
		private IPlayerControlService _playerControlService;
		private PlayerAnimatorController _playerAnimatorController;

		public void Construct(
			Transform camera,
			Transform transform,
			Rigidbody rigidbody,
			FallingHandler fallingHandler,
			IGroundDetector groundDetector,
			IPlayerControlService playerControlService,
			PlayerAnimatorController playerAnimatorController)
		{
			_camera = camera;
			_transform = transform;
			_rigidbody = rigidbody;
			_fallingHandler = fallingHandler;
			_groundDetector = groundDetector;
			_playerControlService = playerControlService;
			_playerAnimatorController = playerAnimatorController;
		}

		public void Update(float deltaTime)
		{
			if (_isFall == false)
			{
				_isFall = true;
				_fallingHandler.ResetAirTime();
				_playerAnimatorController.Falling.Launch();
			}
			
			_fallingHandler.Update(deltaTime);
			_currentHorizontalSpeed = HorizontalSpeed(deltaTime);
			Vector3 horizontalVelocity = HorizontalVelocity();
			Vector3 verticalVelocity = _fallingHandler.VerticalVelocity();
			_rigidbody.velocity = verticalVelocity + horizontalVelocity;
		}

		public void End()
		{
			if (_isFall)
			{
				_isFall = false;
				_playerAnimatorController.Falling.Landing();
			}
		}

		private float HorizontalSpeed(float deltaTime)
		{
			float newSpeed = _playerControlService.IsMovement
				? _settings.HorizontalSpeed
				: 0;

			return Mathf.Lerp(_currentHorizontalSpeed, newSpeed,
				_settings.HorizontalSpeedDamper * deltaTime);
		}

		private Vector3 HorizontalVelocity()
		{
			var velocity = _camera.forward * _playerControlService.MovementDirection.y;
			velocity += _camera.right * _playerControlService.MovementDirection.x;
			velocity = Vector3.ProjectOnPlane(velocity, _transform.up);
			velocity.Normalize();
			velocity *= _currentHorizontalSpeed;
			return velocity;
		}
	}
}