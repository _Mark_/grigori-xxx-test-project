using System;
using Extensions;
using UnityEngine;

namespace Mechanic.Player.Locomotion.Fall
{
	[Serializable]
	public class FallingHandler
	{
		[SerializeField] private float _maxSpeed;
		[SerializeField] private float _maxSpeedTime;
		[SerializeField] private AnimationCurve _fallSpeed;
    
		private float _airTime;
		private Transform _transform;

		public void Construct(Transform transform) => 
			_transform = transform;

		public void ResetAirTime() => 
			_airTime = 0;

		public void Update(float deltaTime) => 
			_airTime += deltaTime;

		public Vector3 VerticalVelocity()
		{
			float time = Mathf.Clamp01(_airTime / _maxSpeedTime);
			var speed = _fallSpeed.Evaluate(time) * _maxSpeed;
			return _transform.Down() * speed;
		}
	}
}