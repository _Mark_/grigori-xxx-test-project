﻿using System;
using Mechanic.Player.Locomotion.Animation;
using UnityEngine;

namespace Mechanic.Player.Locomotion
{
	[Serializable]
	public class Jumping
	{
		[SerializeField] private float _duration = 1;
		[SerializeField] private float _maxSpeed = 10;
		[SerializeField] private AnimationCurve _verticalSpeed;

		private float _timer;
		private Vector3 _horizontalVelocity;
		private Rigidbody _rigidbody;
		private IGroundDetector _groundDetector;
		private PlayerLocomotion _playerLocomotion;
		private PlayerAnimatorController _playerAnimatorController;

		public bool IsEnabled { get; private set; }

		public void Construct(
			Rigidbody rigidbody,
			IGroundDetector groundDetector,
			PlayerLocomotion playerLocomotion,
			PlayerAnimatorController playerAnimatorController)
		{
			_rigidbody = rigidbody;
			_groundDetector = groundDetector;
			_playerLocomotion = playerLocomotion;
			_playerAnimatorController = playerAnimatorController;
		}

		public void Launch()
		{
			if (_playerLocomotion.Walking.IsEnabled == false) return;

			_playerLocomotion.Walking.IsEnabled = false;

			_timer = 0;
			IsEnabled = false;
			_horizontalVelocity =
				Vector3.Scale(_rigidbody.velocity, new Vector3(1, 0, 1));
			_playerAnimatorController.Jumping.Launch();
		}

		public void Update(float deltaTime)
		{
			if (IsEnabled == false) return;

			_timer += deltaTime;

			var relationship = _timer / _duration;
			if (relationship >= 1)
				End();

			var curveSpeedValue = _verticalSpeed.Evaluate(relationship);
			_rigidbody.velocity = CalculateVelocity(curveSpeedValue);
		}

		public void LaunchMoveUp() => 
			IsEnabled = true;

		private Vector3 CalculateVelocity(float curveSpeedValue)
		{
			var verticalSpeed = VerticalSpeed(curveSpeedValue);
			var velocity = new Vector3(_horizontalVelocity.x, verticalSpeed,
				_horizontalVelocity.z);

			return velocity;
		}

		private float VerticalSpeed(float curveSpeedValue)
		{
			if (_groundDetector.IsGrounded && curveSpeedValue < 0)
				return 0;

			var verticalSpeed = curveSpeedValue * _maxSpeed;

			return verticalSpeed;
		}

		private void End()
		{
			IsEnabled = false;
			_playerAnimatorController.Jumping.Landing();
		}
	}
}