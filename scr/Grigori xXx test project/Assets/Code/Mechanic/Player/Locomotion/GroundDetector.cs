using Extensions;
using UnityEngine;

namespace Mechanic.Player.Locomotion
{
	public class GroundDetector : MonoBehaviour, IGroundDetector
	{
		private const float ADDITIONAL_LENGTH_FOR_RAY = .01f;

		[SerializeField] private float _stepHeight;
		[SerializeField] private float _sphereRadiusMultiplier;
		[SerializeField] private LayerMask _contactLayers;

		private Transform _transform;
		private CapsuleCollider _collider;

		public bool IsHasStep { get; private set; }
		public bool IsGrounded { get; private set; }
		public RaycastHit Hit { get; private set; }

		public void Construct(Transform transform, CapsuleCollider collider)
		{
			_collider = collider;
			_transform = transform;
		}

		public void Handle()
		{
			var position = _transform.position;
			var origin = position + _transform.up * _collider.center.y;
			var originHeight = Vector3.Distance(position, origin);

			IsHasStep = CheckStep(origin, originHeight);
			IsGrounded = CheckGrounded(origin, originHeight);
		}

		private bool CheckStep(Vector3 origin, float originHeight)
		{
			float castDistance = originHeight + _stepHeight;
			if (Physics.Raycast(origin, _transform.Down(), out var hit, castDistance,
				    _contactLayers))
			{
				Hit = hit;
				return true;
			}

			return false;
		}

		private bool CheckGrounded(Vector3 origin, float originHeight)
		{
			var ray = new Ray(origin, _transform.Down());
			if (Physics.Raycast(ray, originHeight + ADDITIONAL_LENGTH_FOR_RAY,
				    _contactLayers))
				return true;

			var radius = _collider.radius * _sphereRadiusMultiplier;
			var castDistance = originHeight - radius;
			if (Physics.SphereCast(ray, radius, castDistance, _contactLayers))
				return true;

			return false;
		}

		private RaycastHit ClarifyHit(ref RaycastHit hit, float castDistance)
		{
			Physics.Raycast(hit.point + _transform.up, _transform.Down(), out hit,
				castDistance, _contactLayers);
			return hit;
		}
	}
}