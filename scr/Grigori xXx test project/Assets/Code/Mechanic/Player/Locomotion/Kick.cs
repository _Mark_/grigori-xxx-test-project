﻿using Mechanic.Player.Locomotion.Animation;
using UnityEngine;

namespace Mechanic.Player.Locomotion
{
	public class Kick
	{
		private readonly Rigidbody _rigidbody;
		private readonly PlayerLocomotion _playerLocomotion;
		private readonly PlayerAnimatorController _playerAnimatorController;

		public Kick(
			Rigidbody rigidbody,
			PlayerLocomotion playerLocomotion,
			PlayerAnimatorController playerAnimatorController)
		{
			_rigidbody = rigidbody;
			_playerLocomotion = playerLocomotion;
			_playerAnimatorController = playerAnimatorController;
		}

		public void Launch()
		{
			if (_playerLocomotion.Walking.IsEnabled == false)
				return;

			_rigidbody.velocity = Vector3.zero;
			_playerLocomotion.Walking.IsEnabled = false;
			_playerAnimatorController.Kick.Launch();
		}

		public void End() =>
			_playerLocomotion.Walking.IsEnabled = true;
	}
}