﻿using Mechanic.Player.Animation;
using UnityEngine;

namespace Mechanic.Player.Locomotion.Animation
{
	public class Jumping
	{
		private readonly Animator _animator;
		private readonly StateHash _stateHash;
		private readonly ParameterHash _parameterHash;
		private readonly PlayerLocomotion _playerLocomotion;

		public Jumping(
			Animator animator,
			StateHash stateHash,
			ParameterHash parameterHash,
			PlayerLocomotion playerLocomotion)
		{
			_animator = animator;
			_stateHash = stateHash;
			_parameterHash = parameterHash;
			_playerLocomotion = playerLocomotion;
		}

		public void Launch()
		{
			_animator.SetBool(_parameterHash.Jump, true);
		}

		public void Landing()
		{
			_animator.SetBool(_parameterHash.Jump, false);
			_animator.SetBool(_parameterHash.JumpLanding, true);
		}

		public void End(int shortNameHash)
		{
			if (shortNameHash != _stateHash.JumpLanding) 
				return;
			
			_animator.SetBool(_parameterHash.JumpLanding, false);
			_playerLocomotion.Walking.IsEnabled = true;
		}

		public void StartMoveUp()
		{
			_playerLocomotion.Jumping.LaunchMoveUp();
		}
	}
}