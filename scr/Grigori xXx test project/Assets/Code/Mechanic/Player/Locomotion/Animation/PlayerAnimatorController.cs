﻿using Mechanic.Player.Animation;
using Service.Control.Player;
using UnityEngine;
using Zenject;

namespace Mechanic.Player.Locomotion.Animation
{
	[RequireComponent(typeof(Animator))]
	public class PlayerAnimatorController : MonoBehaviour,
		IAnimationStateExitReader
	{
		private Animator _animator;
		private StateHash _stateHash;
		private ParameterHash _parameterHash;
		private PlayerLocomotion _playerLocomotion;
		private IPlayerControlService _playerControlService;
		private PlayerWeaponAnimation _playerWeaponAnimation;

		public Kick Kick { get; private set; }
		public Falling Falling { get; private set; }
		public Jumping Jumping { get; private set; }

		[Inject]
		private void Construct(
			StateHash stateHash,
			ParameterHash parameter,
			PlayerLocomotion playerLocomotion,
			IPlayerControlService playerControlService,
			PlayerWeaponAnimation playerWeaponAnimation)
		{
			_stateHash = stateHash;
			_parameterHash = parameter;
			_playerLocomotion = playerLocomotion;
			_playerControlService = playerControlService;
			_playerWeaponAnimation = playerWeaponAnimation;
		}

		private void Awake()
		{
			_animator = GetComponent<Animator>();
			Kick = new Kick(_animator, _stateHash, _parameterHash,
				_playerLocomotion);
			Jumping = new Jumping(_animator, _stateHash, _parameterHash,
				_playerLocomotion);
			Falling = new Falling(_animator, _stateHash, _parameterHash,
				_playerLocomotion);
		}

		private void Update()
		{
			var maxWalkingSpeed = _playerLocomotion.WalkingSettings.GetWalkSpeed;
			var currentSpeed = _playerLocomotion.CurrentSpeed;
			var animationWalkSpeed = currentSpeed / maxWalkingSpeed;

			var movement = _playerControlService.MovementDirection.normalized *
			               animationWalkSpeed;
			_animator.SetFloat(_parameterHash.XVelocity, movement.x);
			_animator.SetFloat(_parameterHash.YVelocity, movement.y);
		}

		public void AnimationExit(int shortNameHash)
		{
			Kick.End(shortNameHash);
			Jumping.End(shortNameHash);
			Falling.End(shortNameHash);
			_playerWeaponAnimation.ReloadEnd(shortNameHash);
		}

		private void FootStep() // Animation event.
		{ }

		private void StartMoveUp() => // Animation event.
			Jumping.StartMoveUp();

		private void KickMoment() => // Animation event.
			Kick.CallKickMoment();
	}
}