﻿using System;
using Mechanic.Player.Animation;
using UnityEngine;

namespace Mechanic.Player.Locomotion.Animation
{
	public class Kick
	{
		private readonly Animator _animator;
		private readonly StateHash _stateHash;
		private readonly ParameterHash _parameterHash;
		private readonly PlayerLocomotion _playerLocomotion;

		public event Action KickMoment;

		public Kick(
			Animator animator,
			StateHash stateHash,
			ParameterHash parameterHash,
			PlayerLocomotion playerLocomotion)
		{
			_animator = animator;
			_stateHash = stateHash;
			_parameterHash = parameterHash;
			_playerLocomotion = playerLocomotion;
		}

		public void Launch()
		{
			_animator.SetBool(_parameterHash.Kick, true);
		}

		public void End(int shortNameHash)
		{
			if (shortNameHash != _stateHash.FrontKick) return;

			_animator.SetBool(_parameterHash.Kick, false);
			_playerLocomotion.Kick.End();
		}

		public void CallKickMoment() => 
			KickMoment?.Invoke();
	}
}