﻿using Mechanic.Player.Animation;
using UnityEngine;

namespace Mechanic.Player.Locomotion.Animation
{
	public class Falling
	{
		private readonly Animator _animator;
		private readonly StateHash _stateHash;
		private readonly ParameterHash _parameterHash;
		private readonly PlayerLocomotion _playerLocomotion;

		public Falling(
			Animator animator,
			StateHash stateHash,
			ParameterHash parameterHash,
			PlayerLocomotion playerLocomotion)
		{
			_animator = animator;
			_stateHash = stateHash;
			_parameterHash = parameterHash;
			_playerLocomotion = playerLocomotion;
		}

		public void Launch()
		{
			_animator.SetBool(_parameterHash.Fall, true);
		}

		public void Landing()
		{
			_playerLocomotion.Walking.IsEnabled = false;
			_animator.SetBool(_parameterHash.Fall, false);
			_animator.SetBool(_parameterHash.FallLanding, true);
		}

		public void End(int shortNameHash)
		{
			if (shortNameHash != _stateHash.FallLanding)
				return;

			_playerLocomotion.Walking.IsEnabled = true;
			_animator.SetBool(_parameterHash.FallLanding, false);
		}
	}
}