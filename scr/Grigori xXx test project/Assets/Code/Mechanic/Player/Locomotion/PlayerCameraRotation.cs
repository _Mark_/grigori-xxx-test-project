﻿using Service.Control.Player;
using UnityEngine;
using Zenject;

namespace Mechanic.Player.Locomotion
{
	public class PlayerCameraRotation : MonoBehaviour
	{
		[SerializeField] private Transform _pivot;
		[SerializeField] private Transform _target;
		[SerializeField] private Transform _viewTarget;
		[SerializeField] private float _sensitive = 1;
		[SerializeField] private float _minXAngel = -45;
		[SerializeField] private float _maxXAngle = 30;

		private float _xAngle;
		private IMouseInput _mouseInput;

		[Inject]
		private void Construct(IMouseInput mouseInput)
		{
			_mouseInput = mouseInput;
		}

		public void LateUpdate()
		{
			_xAngle -= _mouseInput.Delta.y * _sensitive * Time.deltaTime;
			_xAngle = Mathf.Clamp(_xAngle, _minXAngel, _maxXAngle);
			
			var yAngel = _pivot.rotation.eulerAngles.y;
			
			_pivot.rotation = Quaternion.Euler(_xAngle, yAngel, 0);

			transform.position = _target.position;
			transform.LookAt(_viewTarget);
		}
	}
}