﻿using System;
using Service.Control.Player;
using UnityEngine;
using Zenject;

namespace Mechanic.Player.Locomotion
{
	public class CharacterRotation : MonoBehaviour
	{
		private IMouseInput _mouseInput;

		[Inject]
		private void Construct(IMouseInput mouseInput)
		{
			_mouseInput = mouseInput;
		}

		private void FixedUpdate()
		{
			RotateCharacter();
		}

		private void RotateCharacter()
		{
			var deltaX = _mouseInput.Delta.x;
			transform.Rotate(0, deltaX, 0);
		}
	}
}