using UnityEngine;

namespace Mechanic.Player.Locomotion
{
	public interface IGroundDetector
	{
		bool IsHasStep { get; }
		bool IsGrounded { get; }
		RaycastHit Hit { get; }
	}
}