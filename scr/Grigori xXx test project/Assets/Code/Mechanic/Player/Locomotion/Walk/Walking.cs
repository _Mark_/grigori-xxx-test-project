using System;
using Service.Control.Player;
using UnityEngine;

namespace Mechanic.Player.Locomotion.Walk
{
	[Serializable]
	public class Walking
	{
		private bool _runMode;

		[SerializeField] private Settings _settings;

		private Transform _camera;
		private Transform _transform;
		private Rigidbody _rigidbody;
		private IPlayerControlService _playerControlService;
		private IGroundDetector _groundDetector;

		public float CurrentSpeed { get; private set; }
		public Settings Settings => _settings;
		public bool IsEnabled { get; set; } = true;

		public void Construct(
			Transform camera,
			Transform transform,
			Rigidbody rigidbody,
			IPlayerControlService playerControlService,
			IGroundDetector groundDetector)
		{
			_camera = camera;
			_transform = transform;
			_rigidbody = rigidbody;
			_playerControlService = playerControlService;
			_groundDetector = groundDetector;
		}

		public void Update(float deltaTime)
		{
			if (IsEnabled == false) return;

			CurrentSpeed = CalculateSpeed(deltaTime);
			var surfaceNormal = _groundDetector.Hit.normal;

			if (IsSlopeAvailable(surfaceNormal))
				Movement(deltaTime, surfaceNormal);
			else
				SlidingAlongSlope(surfaceNormal);
		}

		private void Movement(float deltaTime, Vector3 surfaceNormal)
		{
			ApplyVelocity(surfaceNormal);
			NormalizeVerticalPosition(deltaTime);
		}

		private void SlidingAlongSlope(Vector3 surfaceNormal)
		{
			var slidingDirection =
				Vector3.ProjectOnPlane(surfaceNormal, _transform.up);
			slidingDirection =
				Vector3.ProjectOnPlane(slidingDirection, surfaceNormal);
			_rigidbody.velocity =
				slidingDirection.normalized * _settings.SlidingSpeed;
		}

		private float CalculateSpeed(float deltaTime)
		{
			float newSpeed;
			if (_playerControlService.IsMovement == false)
				newSpeed = 0;
			else
				newSpeed = _settings.WalkSpeed;

			return Mathf.Lerp(CurrentSpeed, newSpeed,
				_settings.SpeedDamper * deltaTime);
		}

		private void ApplyVelocity(Vector3 surfaceNormal)
		{
			var velocity = Velocity(CurrentSpeed, surfaceNormal);
			_rigidbody.velocity = velocity;
		}

		private void NormalizeVerticalPosition(float deltaTime)
		{
			if (IsHasStep())
			{
				var target = _groundDetector.Hit.point;
				_rigidbody.MovePosition(target);
			}
		}

		private Vector3 Velocity(float speed, Vector3 surfaceNormal)
		{
			var velocity =
				_camera.forward * _playerControlService.MovementDirection.y;
			velocity += _camera.right * _playerControlService.MovementDirection.x;
			velocity = Vector3.ProjectOnPlane(velocity, surfaceNormal);
			velocity.Normalize();
			velocity *= speed;
			return velocity;
		}

		private void OnRunModePressed() =>
			_runMode = !_runMode;

		private bool IsSlopeAvailable(Vector3 surfaceNormal) =>
			Vector3.Angle(surfaceNormal, _transform.up) < _settings.AvailableSlope;

		private bool IsHasStep() =>
			_groundDetector.IsHasStep;
	}
}