﻿namespace Mechanic.Player.Locomotion.Walk
{
	public interface IWalkingSettings
	{
		float GetWalkSpeed { get; }
		float GetSpeedDamper { get; }
		float GetSlidingSpeed { get; }
		float GetAvailableSlope { get; }
	}
}