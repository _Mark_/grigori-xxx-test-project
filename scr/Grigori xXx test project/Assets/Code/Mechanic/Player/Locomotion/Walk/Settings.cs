﻿using System;
using UnityEngine;

namespace Mechanic.Player.Locomotion.Walk
{
	[Serializable]
	public class Settings : IWalkingSettings
	{
		public float WalkSpeed = 5;
		[Space]
		public float SpeedDamper = 1;
		[Header("Slope")]
		public float SlidingSpeed = 10;
		public float AvailableSlope = 45;
		
		public float GetWalkSpeed => WalkSpeed;
		public float GetSpeedDamper => SpeedDamper;
		public float GetSlidingSpeed => SlidingSpeed;
		public float GetAvailableSlope => AvailableSlope;
	}
}