using Mechanic.Player.Locomotion.Animation;
using Mechanic.Player.Locomotion.Fall;
using Mechanic.Player.Locomotion.Walk;
using Service.Control.Player;
using UnityEngine;
using Zenject;
using Falling = Mechanic.Player.Locomotion.Fall.Falling;

namespace Mechanic.Player.Locomotion
{
	[RequireComponent(typeof(Rigidbody), typeof(CapsuleCollider))]
	public class PlayerLocomotion : MonoBehaviour
	{
		[SerializeField] private Transform _camera;
		[SerializeField] private GroundDetector _groundDetector;
		[Space]
		[SerializeField] private Walking _walking;
		[SerializeField] private Falling _falling;
		[SerializeField] private Jumping _jumping;
		[SerializeField] private FallingHandler _fallingHandler;

		private Kick _kick;
		private IPlayerControlService _playerControlService;
		private PlayerAnimatorController _playerAnimatorController;

		public Kick Kick => _kick;
		public float CurrentSpeed => _walking.CurrentSpeed;
		public Walking Walking => _walking;
		public IWalkingSettings WalkingSettings => _walking.Settings;
		public Jumping Jumping => _jumping;

		[Inject]
		private void Construct(
			IPlayerControlService playerControlService,
			PlayerAnimatorController playerAnimatorController)
		{
			_playerControlService = playerControlService;
			_playerAnimatorController = playerAnimatorController;
		}

		private void Awake()
		{
			var rigidbody = GetComponent<Rigidbody>();
			var collider = GetComponent<CapsuleCollider>();

			_kick = new Kick(rigidbody, this, _playerAnimatorController);
			_groundDetector.Construct(transform, collider);
			_walking.Construct(_camera, transform, rigidbody, _playerControlService,
				_groundDetector);
			_fallingHandler.Construct(transform);
			_falling.Construct(_camera, transform, rigidbody, _fallingHandler,
				_groundDetector, _playerControlService, _playerAnimatorController);
			_jumping.Construct(rigidbody, _groundDetector, this, _playerAnimatorController);
		}

		private void FixedUpdate()
		{
			_groundDetector.Handle();

			var deltaTime = Time.deltaTime;

			if (_groundDetector.IsGrounded)
			{
				_walking.Update(deltaTime);
				_falling.End();
			}
			else if (_jumping.IsEnabled == false)
			{
				_falling.Update(deltaTime);
			}
			
			_jumping.Update(deltaTime);
		}
	}
}