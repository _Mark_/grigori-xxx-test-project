﻿using System.Linq;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Extensions
{
	public static class EventTriggerExtension
	{
		public static EventTrigger AddListener(
			this EventTrigger trigger,
			EventTriggerType triggerType,
			UnityAction<BaseEventData> callback)
		{
			var entry = NewEntry(triggerType);
			entry.callback.AddListener(callback);
			trigger.triggers.Add(entry);
			return trigger;
		}

		public static EventTrigger RemoveListener(
			this EventTrigger trigger,
			EventTriggerType triggerType,
			UnityAction<BaseEventData> callback)
		{
			var entry =
				trigger.triggers.FirstOrDefault(entry => entry.eventID == triggerType);
			
			if (entry == default) return trigger;
			
			entry.callback.RemoveListener(callback);
			
			return trigger;
		}

		public static EventTrigger.Entry NewEntry(EventTriggerType triggerType)
		{
			var newEntry = NewEntry();
			newEntry.eventID = triggerType;
			return newEntry;
		}

		public static EventTrigger.Entry NewEntry() =>
			new EventTrigger.Entry();
	}
}