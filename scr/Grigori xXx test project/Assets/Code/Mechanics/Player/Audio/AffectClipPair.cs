﻿using System;
using UnityEngine;

namespace Mechanics.Player.Audio
{
	[Serializable]
	public class AffectClipPair
	{
		public EAudioFX Effect;
		public AudioClip Clip;
	}
}