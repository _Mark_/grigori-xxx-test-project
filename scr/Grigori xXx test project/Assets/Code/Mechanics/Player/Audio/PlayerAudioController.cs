﻿using System.Collections.Generic;
using UnityEngine;

namespace Mechanics.Player.Audio
{
	public class PlayerAudioController : MonoBehaviour
	{
		[SerializeField] private AffectClipPair[] _collection;

		public Dictionary<EAudioFX, AudioClip> AudioCollection { get; } =
			new Dictionary<EAudioFX, AudioClip>();

		private void Awake()
		{
			for (var i = 0; i < _collection.Length; i++)
			{
				var effect = _collection[i].Effect;
				var clip = _collection[i].Clip;
				AudioCollection.Add(effect, clip);
			}

			_collection = null;
		}
	}
}