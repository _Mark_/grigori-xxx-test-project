﻿namespace Mechanics.Player.Weapon
{
	public interface IWeapon
	{
		string Name { get; }
		void Fire();
	}
}