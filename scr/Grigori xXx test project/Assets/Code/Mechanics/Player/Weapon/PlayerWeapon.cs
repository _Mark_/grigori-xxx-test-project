﻿using System;
using System.Collections.Generic;
using System.Linq;
using Service.Control.Player;
using UnityEngine;
using Zenject;

namespace Mechanics.Player.Weapon
{
	public class PlayerWeapon : MonoBehaviour
	{
		public class Weapon
		{
			public IWeapon Functional;
			public Transform LeftHandTarget;
			public WeaponData Data;
			public GameObject Object;
		}

		[SerializeField] private Transform[] _leftHandTargets;
		[SerializeField] private GameObject[] _weaponObjects;
		[SerializeField] private WeaponData[] _weaponDataset;

		private int _currentWeaponIndex;
		private List<IWeapon> _weaponList;
		private IPlayerControlService _playerControlService;

		public event Action WeaponChanged;
		
		public Weapon CurrentWeapon { get; } = new();

		[Inject]
		private void Construct(
			List<IWeapon> weaponList,
			IPlayerControlService playerControlService)
		{
			_weaponList = weaponList;
		}

		private void Awake()
		{
			_currentWeaponIndex = _currentWeaponIndex >= _weaponList.Count
				? 0
				: _currentWeaponIndex;

			SetCurrentWeapon();
		}

		public void Fire()
		{
			CurrentWeapon.Functional.Fire();
		}

		public void SelectNextWeapon()
		{
			SetNewWeaponIndex();
			SetCurrentWeapon();
			WeaponChanged?.Invoke();
		}

		private void SetNewWeaponIndex()
		{
			if (_currentWeaponIndex + 1 >= _weaponList.Count)
				_currentWeaponIndex = 0;
			else
				_currentWeaponIndex++;
		}

		private void SetCurrentWeapon()
		{
			CurrentWeapon.Object = _weaponObjects[_currentWeaponIndex];
			CurrentWeapon.Functional = _weaponList[_currentWeaponIndex];
			CurrentWeapon.LeftHandTarget = _leftHandTargets[_currentWeaponIndex];
			CurrentWeapon.Data = _weaponDataset
				.First(data => data.Name == CurrentWeapon.Functional.Name);
		}
	}
}