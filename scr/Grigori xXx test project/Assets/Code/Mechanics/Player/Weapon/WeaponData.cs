﻿using UnityEngine;

namespace Mechanics.Player.Weapon
{
	[CreateAssetMenu(menuName = "Weapon/Properties")]
	public class WeaponData : ScriptableObject
	{
		public string Name;
		public Sprite Sprite;
		[Space]
		public Vector3 HandPosition;
		public Vector3 HandRotation;
	}
}