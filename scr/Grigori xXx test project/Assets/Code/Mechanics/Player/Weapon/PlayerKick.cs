﻿using System;
using Mechanic.Player.Locomotion.Animation;
using Mechanics.Health;
using UnityEngine;
using Zenject;

namespace Mechanics.Player.Weapon
{
	public sealed class PlayerKick : MonoBehaviour
	{
		public bool IsDrawGizmos = true;
		[Space]
		[SerializeField] private LayerMask _contactLayers;
		[Space]
		[SerializeField] private int _damageValue = 5;
		[SerializeField] private int _heartColliderCount = 5;
		[Space]
		[SerializeField] private float _hitSphereRadius = .5f;
		[SerializeField] private Transform _hitSphereCenter;

		private Collider[] _heartColliders;
		private PlayerAnimatorController _playerAnimatorController;

		public event Action TakenDamage;

		[Inject]
		private void Construct(PlayerAnimatorController playerAnimatorController)
		{
			_playerAnimatorController = playerAnimatorController;
		}

		private void Awake()
		{
			_heartColliders = new Collider[_heartColliderCount];
		}

		private void Start()
		{
			_playerAnimatorController.Kick.KickMoment += OnKickMoment;
		}

		private void OnDestroy()
		{
			_playerAnimatorController.Kick.KickMoment -= OnKickMoment;
		}

		private void DealDamage()
		{
			var hitCount = Physics.OverlapSphereNonAlloc(
				_hitSphereCenter.position, _hitSphereRadius, _heartColliders,
				_contactLayers);

			if (hitCount == 0) return;

			HandleHits(hitCount);
		}

		private void HandleHits(int hitCount)
		{
			var isHasBeenKick = false;

			for (int i = 0; i < hitCount; i++)
			{
				if (_heartColliders[i].TryGetComponent<IHeartBox>(out var heartBox))
				{
					if (heartBox.Health.IsDead)
						continue;

					heartBox.Health.TakeDamage(_damageValue);
					isHasBeenKick = true;
				}
			}

			if (isHasBeenKick)
				TakenDamage?.Invoke();
		}

		private void OnKickMoment() =>
			DealDamage();

		private void OnDrawGizmos()
		{
			if (IsDrawGizmos == false
			    || _hitSphereCenter == null)
				return;

			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(_hitSphereCenter.position, _hitSphereRadius);
		}
	}
}