﻿using Mechanics.Player.Audio;
using UnityEngine;
using Zenject;

namespace Mechanics.Player.Weapon
{
	[RequireComponent(typeof(AudioSource))]
	public class PlayerKickAudio : MonoBehaviour
	{
		[SerializeField] private EAudioFX _kickAudio = EAudioFX.Kick;
		
		private PlayerKick _playerKick;
		private AudioSource _audioSource;
		private PlayerAudioController _playerAudioController;

		[Inject]
		private void Construct(
			PlayerKick playerKick,
			PlayerAudioController playerAudioController)
		{
			_playerKick = playerKick;
			_playerAudioController = playerAudioController;
		}

		private void Awake()
		{
			_audioSource = GetComponent<AudioSource>();
			_audioSource.loop = false;
			_audioSource.playOnAwake = false;

			_playerKick.TakenDamage += OnKickTakenDamage;
		}

		private void OnDestroy()
		{
			_playerKick.TakenDamage -= OnKickTakenDamage;
		}

		private void OnKickTakenDamage()
		{
			var clip = _playerAudioController.AudioCollection[_kickAudio];
			_audioSource.PlayOneShot(clip);
		}
	}
}