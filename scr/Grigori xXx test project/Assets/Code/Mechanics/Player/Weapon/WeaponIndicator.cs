﻿using UnityEngine;
using UnityEngine.UI;

namespace Mechanics.Player.Weapon
{
	public class WeaponIndicator : MonoBehaviour
	{
		[SerializeField] private Image _weaponImage;
		[SerializeField] private PlayerWeapon _playerWeapon;

		private void Awake()
		{
			_playerWeapon.WeaponChanged += OnWeaponChanged;
		}

		private void OnDestroy()
		{
			_playerWeapon.WeaponChanged -= OnWeaponChanged;
		}

		private void Start() =>
			SetWeaponSprite();

		private void OnWeaponChanged() =>
			SetWeaponSprite();

		private void SetWeaponSprite()
		{
			_weaponImage.sprite = _playerWeapon.CurrentWeapon.Data.Sprite;
		}
	}
}