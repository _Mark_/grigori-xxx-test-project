﻿using UnityEngine;

namespace Mechanics.Player.Weapon
{
	public class M4 : MonoBehaviour, IWeapon
	{
		[SerializeField] private string _name = "M4";

		public string Name => _name;

		public void Fire()
		{
			throw new System.NotImplementedException();
		}
	}
}