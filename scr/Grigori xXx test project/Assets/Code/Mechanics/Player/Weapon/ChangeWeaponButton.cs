﻿using Service.Control.Player;
using UnityEngine;
using Zenject;

namespace Mechanics.Player.Weapon
{
	public class ChangeWeaponButton : MonoBehaviour
	{
		[SerializeField] private PlayerWeapon _playerWeapon;
		
		private IPlayerControlService _playerControlService;

		[Inject]
		private void Construct(IPlayerControlService playerControlService) => 
			_playerControlService = playerControlService;

		private void Awake() => 
			_playerControlService.NextWeapon += OnWeaponChange;

		private void OnDestroy() => 
			_playerControlService.NextWeapon -= OnWeaponChange;

		private void OnWeaponChange() => 
			_playerWeapon.SelectNextWeapon();
	}
}