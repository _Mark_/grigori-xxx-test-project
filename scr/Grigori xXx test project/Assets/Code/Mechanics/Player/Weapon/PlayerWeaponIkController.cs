﻿using UnityEngine;
using Zenject;

namespace Mechanics.Player.Weapon
{
	public class PlayerWeaponIkController : MonoBehaviour
	{
		[SerializeField] private Animator _animator;
		[Space]
		[SerializeField] private Transform _targetLook;


		[SerializeField] private float handsWeight = 1;
		[SerializeField] private float _bodyWeight = .5f;
		[SerializeField] private float _headWeight = 1;
		[Space]
		[SerializeField] private float _rightHandPositionWeight = 1;
		[SerializeField] private float _rightHandRotationWeight = 1;
		[SerializeField] private float _leftHandPositionWeight = 1;
		[SerializeField] private float _leftHandRotationWeight = 1;

		private Transform _aimPivot;
		private Transform _leftHand;
		private Transform _rightHand;
		private Transform _shoulderBone;
		private PlayerWeapon _playerWeapon;

		[Inject]
		private void Construct(PlayerWeapon playerWeapon)
		{
			_playerWeapon = playerWeapon;
		}

		private void Start()
		{
			_shoulderBone = _animator.GetBoneTransform(HumanBodyBones.RightShoulder);

			_aimPivot = new GameObject("aim pivot").transform;
			_aimPivot.parent = transform;

			_rightHand = new GameObject("right hand").transform;
			_rightHand.parent = _aimPivot;

			_leftHand = new GameObject("left hand").transform;
			_leftHand.parent = _aimPivot;

			_rightHand.localPosition =
				_playerWeapon.CurrentWeapon.Data.HandPosition;
			_rightHand.localRotation =
				Quaternion.Euler(_playerWeapon.CurrentWeapon.Data.HandRotation);
		}

		private void Update()
		{
			_leftHand.position = _playerWeapon.CurrentWeapon.LeftHandTarget.position;
			_leftHand.rotation = _playerWeapon.CurrentWeapon.LeftHandTarget.rotation;
		}

		private void OnAnimatorIK(int layerIndex)
		{
			_aimPivot.position = _shoulderBone.position;
			_aimPivot.LookAt(_targetLook);

			_animator.SetLookAtWeight(handsWeight, _bodyWeight, _headWeight);
			_animator.SetLookAtPosition(_targetLook.position);

			_animator.SetIKPositionWeight(AvatarIKGoal.LeftHand,
				_leftHandPositionWeight);
			_animator.SetIKRotationWeight(AvatarIKGoal.LeftHand,
				_leftHandRotationWeight);
			_animator.SetIKPosition(AvatarIKGoal.LeftHand, _leftHand.position);
			_animator.SetIKRotation(AvatarIKGoal.LeftHand, _leftHand.rotation);

			_animator.SetIKPositionWeight(AvatarIKGoal.RightHand,
				_rightHandPositionWeight);
			_animator.SetIKRotationWeight(AvatarIKGoal.RightHand,
				_rightHandRotationWeight);
			_animator.SetIKPosition(AvatarIKGoal.RightHand, _rightHand.position);
			_animator.SetIKRotation(AvatarIKGoal.RightHand, _rightHand.rotation);
		}

		public float HandsWeight
		{
			get => _leftHandPositionWeight;
			set
			{
				_rightHandPositionWeight = value;
				_rightHandRotationWeight = value;
				_leftHandPositionWeight = value;
				_leftHandRotationWeight = value;
			}
		}
	}
}