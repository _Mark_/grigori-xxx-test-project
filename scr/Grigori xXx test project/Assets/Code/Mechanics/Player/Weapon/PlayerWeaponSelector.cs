﻿using UnityEngine;
using Zenject;

namespace Mechanics.Player.Weapon
{
	public class PlayerWeaponSelector : MonoBehaviour
	{
		private GameObject _weaponObjet;
		private PlayerWeapon _playerWeapon;

		[Inject]
		private void Construct(PlayerWeapon playerWeapon)
		{
			_playerWeapon = playerWeapon;
			_playerWeapon.WeaponChanged += OnPlayerWeaponChanged;
		}

		private void OnDestroy()
		{
			_playerWeapon.WeaponChanged -= OnPlayerWeaponChanged;
		}

		private void Start()
		{
			ActivateNewWeapon();
		}

		private void OnPlayerWeaponChanged()
		{
			_weaponObjet.SetActive(false);
			ActivateNewWeapon();
		}

		private void ActivateNewWeapon()
		{
			_weaponObjet = _playerWeapon.CurrentWeapon.Object;
			_weaponObjet.SetActive(true);
		}
	}
}