﻿using System;
using UnityEngine;

namespace Mechanics.Health
{
	public class Health : MonoBehaviour, IHealth
	{
		[SerializeField] private bool _isDead;
		[SerializeField] private int _value;
		[SerializeField] private int _min;
		[SerializeField] private int _max;

		public int Value => _value;
		public int Min => _min;
		public int Max => _max;

		public event Action Death;
		public event EventHandler<TakenHealArgs> TakenHeal;
		public event EventHandler<TakenDamageArgs> TakenDamage;

		public bool IsDead => _isDead;

		public void Heal(int heal)
		{
			_value += heal;
			if (_value > _max)
				_value = _max;

			TakenHeal?.Invoke(this, new TakenHealArgs(heal));
		}

		public void TakeDamage(int damage)
		{
			_value -= damage;
			if (_value < _min)
			{
				Death?.Invoke();
				return;
			}

			TakenDamage?.Invoke(this, new TakenDamageArgs(damage));
		}
	}
}