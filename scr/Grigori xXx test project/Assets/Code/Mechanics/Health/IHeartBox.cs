﻿namespace Mechanics.Health
{
	public interface IHeartBox
	{
		IHealth Health { get; }
	}
}