﻿using System;

namespace Mechanics.Health
{
	public sealed class TakenHealArgs : EventArgs
	{
		public int HealValueValue;

		public TakenHealArgs(int healValue)
		{
			HealValueValue = healValue;
		}
	}
}