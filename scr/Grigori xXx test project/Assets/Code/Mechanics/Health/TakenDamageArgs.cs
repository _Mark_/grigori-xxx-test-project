﻿using System;

namespace Mechanics.Health
{
	public sealed class TakenDamageArgs : EventArgs
	{
		public int DamageValueValue;

		public TakenDamageArgs(int damageValue)
		{
			DamageValueValue = damageValue;
		}
	}
}