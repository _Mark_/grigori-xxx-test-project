﻿using Mechanics.Health;
using UnityEngine;

namespace Mechanics.Npc
{
	[RequireComponent(typeof(Mechanics.Health.Health))]
	public sealed class NpcHearArea : MonoBehaviour, IHeartBox
	{
		private Health.Health _health;

		public IHealth Health => _health;

		private void Awake()
		{
			_health = GetComponent<Health.Health>();
		}
	}
}