﻿using System;
using UnityEngine;

namespace Service.Control.Car
{
	public class CarStandardControlService : MonoBehaviour, ICarControlService
	{
		public event Action LeftPressed;
		public event Action LeftUnPressed;
		public event Action RightPressed;
		public event Action RightUnPressed;
		public event Action GazPressed;
		public event Action GazUnPressed;
		public event Action GetOutOfTheCar;
		public event Action SwitchMap;
		public event Action ApplyWeapon;
	}
}