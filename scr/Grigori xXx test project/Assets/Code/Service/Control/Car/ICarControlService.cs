﻿using System;

namespace Service.Control.Car
{
	public interface ICarControlService
	{
		event Action LeftPressed;
		event Action LeftUnPressed;
		event Action RightPressed;
		event Action RightUnPressed;
		event Action GazPressed;
		event Action GazUnPressed;
		event Action GetOutOfTheCar;
		event Action SwitchMap;
		event Action ApplyWeapon;
	}
}