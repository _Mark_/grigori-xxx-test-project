using UnityEngine;
using UnityEngine.EventSystems;

namespace Service.Control.Player
{
	public class DragInput : MonoBehaviour, IMouseInput, IDragHandler,
		IEndDragHandler
	{
		[SerializeField] private float _deathZone = 1;
		[SerializeField] private Vector2 _inputVector;

		public Vector2 Delta => _inputVector;

		public void OnEndDrag(PointerEventData eventData)
		{
			_inputVector = Vector3.zero;
		}

		public void OnDrag(PointerEventData eventData)
		{
			if (eventData.delta.magnitude < _deathZone)
			{
				_inputVector = Vector2.zero;
				return;
			}

			_inputVector = eventData.delta;
		}
	}
}