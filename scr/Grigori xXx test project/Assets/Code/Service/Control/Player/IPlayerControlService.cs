﻿using System;
using UnityEngine;

namespace Service.Control.Player
{
	public interface IPlayerControlService
	{
		event Action Kick;
		event Action Jump;
		event Action ReloadWeapon;
		event Action NextWeapon;

		bool IsMovement { get; }
		Vector2 MovementDirection { get; }
	}
}