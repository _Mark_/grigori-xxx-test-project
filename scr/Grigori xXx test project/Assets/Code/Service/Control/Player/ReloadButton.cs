﻿using Mechanic.Player.Animation;
using UnityEngine;
using Zenject;

namespace Service.Control.Player
{
	public class ReloadButton : MonoBehaviour
	{
		private IPlayerControlService _playerControlService;
		private PlayerWeaponAnimation _weaponAnimation;

		[Inject]
		private void Construct(
			IPlayerControlService playerControlService,
			PlayerWeaponAnimation playerWeaponAnimation)
		{
			_playerControlService = playerControlService;
			_weaponAnimation = playerWeaponAnimation;
		}

		private void Awake()
		{
			_playerControlService.ReloadWeapon += OnReloadWeapon;
		}

		private void OnDestroy()
		{
			_playerControlService.ReloadWeapon -= OnReloadWeapon;
		}

		private void OnReloadWeapon()
		{
			_weaponAnimation.LaunchReload();
		}
	}
}