using UnityEngine;

namespace Service.Control.Player
{
	public interface IMouseInput
	{
		Vector2 Delta { get; }
	}
}