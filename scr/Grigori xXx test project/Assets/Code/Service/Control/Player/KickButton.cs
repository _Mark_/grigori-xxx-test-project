﻿using Mechanic.Player.Locomotion;
using UnityEngine;
using Zenject;

namespace Service.Control.Player
{
	public class KickButton : MonoBehaviour
	{
		[SerializeField] private PlayerLocomotion _playerLocomotion;

		private IPlayerControlService _playerControlService;

		[Inject]
		private void Construct(IPlayerControlService playerControlService)
		{
			_playerControlService = playerControlService;
		}

		private void Awake()
		{
			_playerControlService.Kick += OnKick;
		}

		private void OnDestroy()
		{
			_playerControlService.Kick -= OnKick;
		}

		private void OnKick() =>
			_playerLocomotion.Kick.Launch();
	}
}