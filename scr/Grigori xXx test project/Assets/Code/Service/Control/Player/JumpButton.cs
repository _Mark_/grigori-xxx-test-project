﻿using Mechanic.Player.Locomotion;
using UnityEngine;
using Zenject;

namespace Service.Control.Player
{
	public class JumpButton : MonoBehaviour
	{
		[SerializeField] private PlayerLocomotion _playerLocomotion; 
			
		private IPlayerControlService _playerControlService;

		[Inject]
		private void Construct(IPlayerControlService playerControlService)
		{
			_playerControlService = playerControlService;
		}

		private void Awake()
		{
			_playerControlService.Jump += OnJump;
		}

		private void OnDestroy()
		{
			_playerControlService.Jump -= OnJump;
		}

		private void OnJump()
		{
			_playerLocomotion.Jumping.Launch();
		}
	}
}