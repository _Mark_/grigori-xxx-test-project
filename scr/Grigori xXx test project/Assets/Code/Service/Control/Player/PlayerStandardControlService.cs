﻿using System;
using Extensions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Service.Control.Player
{
	public class PlayerStandardControlService : MonoBehaviour,
		IPlayerControlService
	{
		[SerializeField] private Joystick _joystick;
		[Space]
		[SerializeField] private Button _kickButton;
		[SerializeField] private Button _reloadButton;
		[SerializeField] private Button _nextWeaponButton;
		[SerializeField] private EventTrigger _jumpButton;

		public event Action Kick;
		public event Action Jump;
		public event Action SitToAuto;
		public event Action ReloadWeapon;

		public event Action NextWeapon;
		public event Action PreviousWeapon;

		public event Action ApplyWeaponPressed;
		public event Action ApplyWeaponUnPressed;

		public Vector2 Rotate { get; }

		public bool IsMovement => MovementDirection != Vector2.zero;
		public Vector2 MovementDirection => _joystick.Direction;

		private void Awake()
		{
			SubscribeOnEvents();
		}

		private void OnDestroy()
		{
			UnsubscribeFromEvents();
		}

		private void SubscribeOnEvents()
		{
			_kickButton.onClick.AddListener(OnKickButtonClick);
			_reloadButton.onClick.AddListener(OnReloadButtonClick);
			_nextWeaponButton.onClick.AddListener(OnChangeWeaponButtonClick);
			_jumpButton.AddListener(EventTriggerType.PointerDown, OnJumpButtonDown);
		}

		private void UnsubscribeFromEvents()
		{
			_kickButton.onClick.RemoveListener(OnKickButtonClick);
			_reloadButton.onClick.RemoveListener(OnReloadButtonClick);
			_nextWeaponButton.onClick.RemoveListener(OnChangeWeaponButtonClick);
			_jumpButton.RemoveListener(EventTriggerType.PointerDown, OnJumpButtonDown);
		}

		private void OnChangeWeaponButtonClick() => 
			NextWeapon?.Invoke();

		private void OnReloadButtonClick() =>
			ReloadWeapon?.Invoke();

		private void OnKickButtonClick() =>
			Kick?.Invoke();

		private void OnJumpButtonDown(BaseEventData data) => 
			Jump?.Invoke();
	}
}